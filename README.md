projectdoc Developer Diaries
============================


## Overview

This is a free add-on for the [projectdoc Toolbox](https://www.smartics.eu/confluence/display/PDAC1/) for Confluence.

It provides the blueprints to create pages for

  * Diaries, Years, Weeks, Days
  * Assets, Events
  * Todos
  * and more!

## Fork me!
Feel free to fork this project to adjust the templates according to your requirements.

The projectdoc add-on is licensed under [Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

## Documentation

For more information please visit

  * the [add-on's homepage](https://www.smartics.eu/confluence/display/PDAC1/projectdoc+Developer+Diaries)
  * the [add-on on the Atlassian Marketplace](https://marketplace.atlassian.com/plugins/de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-devdiary)
