/*
 * Copyright 2014-2018 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.devdiary;

import de.smartics.projectdoc.atlassian.confluence.admin.space.SpaceAdminConsole;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.BlueprintContextNames;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderHelper;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderSupportService;
import de.smartics.projectdoc.atlassian.confluence.util.PageTitleReplacements;
import com.atlassian.confluence.core.ContentPropertyManager;

import com.atlassian.confluence.core.DateFormatter;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.user.User;

import org.apache.commons.lang3.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Provides information for a day. Since the user does not provide the name and
 * short description required by all projectdoc documents, this information is
 * calculated.
 */
public class ProjectDocWeekContextProvider
    extends AbstractProjectDocContextProvider {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * The identifier to reference the current user's full name.
   */
  public static final String USER_FULL_NAME =
      "projectdoc_doctype_common_userFullName";

  // --- members --------------------------------------------------------------

  private ContentPropertyManager contentPropertyManager;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public ProjectDocWeekContextProvider(
      final ContextProviderSupportService support,
      final ContentPropertyManager contentPropertyManager) {
    super(support);
    this.contentPropertyManager = contentPropertyManager;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  protected BlueprintContext updateBlueprintContext(
      final BlueprintContext blueprintContext) {
    final User user = AuthenticatedUserThreadLocal.get();
    if (user != null) {
      final ContextProviderHelper handler = createHandler(blueprintContext);
      final String team =
          handler.getRenderedNormalizedPropertyByKey(BlueprintContextNames.TEAM, null);
      final String userFullname =
          StringUtils.isNotBlank(team) ? team : user.getFullName();
      blueprintContext.put(USER_FULL_NAME, userFullname);

      final String userName =
          StringUtils.isNotBlank(team) ? team : user.getName();
      final DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
      final Date date = createDate(blueprintContext);
      final String titleBase = calcTitleBase(blueprintContext, userName);
      final String formattedDate = format.format(date);

      if (StringUtils.isBlank(Objects
          .toString(blueprintContext.get(BlueprintContextNames.SHORT_DESCRIPTION), null))) {
        final I18NBean i18n = this.user.createI18n();
        final String shortDescriptionLabel =
            i18n.getText("projectdoc.doctype.week.shortDescription.label",
                new Object[] {userFullname, formattedDate});
        blueprintContext.put(BlueprintContextNames.SHORT_DESCRIPTION, shortDescriptionLabel);
      }

      final WeekInfo weekInfo = addQueryParameterValues(blueprintContext,
          format, date, formattedDate, userName);
      final String name = calcDocumentNameAndSetTitleAndSortKey(
          blueprintContext, weekInfo, titleBase, date);
      blueprintContext.put(BlueprintContextNames.NAME, name);

      final DateFormatter formatter = this.user.getDateFormatter();
      final String dateString = formatter.format(date);
      blueprintContext.put(BlueprintContextNames.CREATION_DATE, dateString);
      final String timeString = formatter.formatTime(date);
      blueprintContext.put(BlueprintContextNames.CREATION_TIME, timeString);

      addSpaceKeyElementContext(blueprintContext);
    }

    updateContextFinally(blueprintContext);

    return blueprintContext;
  }

  private String calcTitleBase(final BlueprintContext blueprintContext,
      final String defaultTitleBase) {
    final String name = Objects.toString(blueprintContext.get(BlueprintContextNames.NAME));
    if (StringUtils.isNotBlank(name)) {
      return name;
    }
    return defaultTitleBase;
  }

  private Date createDate(final BlueprintContext blueprintContext) {
    final Object dateObject = blueprintContext.get("day-date");

    final Date date;
    if (dateObject != null) {
      final DateFormat format = new SimpleDateFormat("yy-MM-dd");
      try {
        date = format.parse(String.valueOf(dateObject));
      } catch (final ParseException e) {
        return new Date();
      }
    } else {
      date = new Date();
    }

    return date;
  }

  private WeekInfo addQueryParameterValues(
      final BlueprintContext blueprintContext, final DateFormat dashedFormat,
      final Date date, final String formattedDate, final String userName) {
    final Calendar calendar = user.createCalendar();
    calendar.setTime(date);
    final int weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);

    final String weekOfYearString = String.valueOf(weekOfYear);
    blueprintContext.put("projectdoc_doctype_week_number", weekOfYearString);

    final DateFormat format = new SimpleDateFormat("yyyyMMdd");
    calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
    final Date weekStart = calendar.getTime();
    final String weekStartString = format.format(weekStart);
    calendar.add(Calendar.DATE, 6);
    final Date weekEnd = calendar.getTime();
    final String weekEndString = format.format(weekEnd);
    blueprintContext.put("projectdoc_week_firstDay", weekStartString);
    blueprintContext.put("projectdoc_week_lastDay", weekEndString);

    addJiraQueryParameterValues(blueprintContext, dashedFormat, weekStart,
        weekEnd, userName);

    final WeekInfo weekInfo = new WeekInfo(calendar, weekOfYear, weekStart);
    return weekInfo;
  }

  private void addJiraQueryParameterValues(
      final BlueprintContext blueprintContext, final DateFormat format,
      final Date weekStart, final Date weekEnd, final String userName) {
    final String spaceKey = blueprintContext.getSpaceKey();
    final SpaceAdminConsole console =
        spaceAdminCenter.createConsoleFor(spaceKey);

    final String jiraName = console.getPropertyAsString("jira.name");
    if (jiraName != null) {
      blueprintContext.put("projectdoc_jira_name", jiraName);

      blueprintContext.put("projectdoc_user_id", userName);

      final String weekStartString = format.format(weekStart);
      final String weekEndString = format.format(weekEnd);
      blueprintContext.put("projectdoc_jira_startDate", weekStartString);
      blueprintContext.put("projectdoc_jira_endDate", weekEndString);
    }
  }

  private static final class WeekInfo {
    private final Calendar calendar;

    private final int weekOfYear;

    private final Date weekStart;

    private final String year;

    private WeekInfo(final Calendar calendar, final int weekOfYear,
        final Date weekStart) {
      this.calendar = calendar;
      this.weekOfYear = weekOfYear;
      this.weekStart = weekStart;
      this.year = calcYear();
    }

    private String calcYear() {
      calendar.setTime(weekStart);
      int year = calendar.get(Calendar.YEAR);

      final int month = calendar.get(Calendar.MONTH);
      if (month == Calendar.DECEMBER) {
        final int day = calendar.get(Calendar.DAY_OF_MONTH);
        if (day >= 29) {
          year += 1;
        }
      }

      return String.valueOf(year);
    }
  }

  private String calcDocumentNameAndSetTitleAndSortKey(
      final BlueprintContext blueprintContext, final WeekInfo weekInfo,
      final String documentName, final Date date) {
    final String adjustedDocumentName;
    if (documentName.indexOf('@') != -1) {
      final Page stub = new Page();
      final Map<String, Object> context = new HashMap<>();

      final Long parentPageId = getAsLong(blueprintContext, "parentPageId");
      if (parentPageId != null) {
        final Page parentPage = pageManager.getPage(parentPageId);
        if (parentPage != null) {
          stub.setParentPage(parentPage);
          supplyTeamToContext(context, parentPage);
        }
      }
      final Space space = spaceManager.getSpace(blueprintContext.getSpaceKey());
      stub.setSpace(space);

      stub.setTitle(documentName);
      final PageTitleReplacements replacer =
          new PageTitleReplacements(spaceManager, contentPropertyManager);
      adjustedDocumentName = replacer.replaceAll(stub, context, date, user);
    } else {
      adjustedDocumentName =
          weekInfo.year + " #" + weekInfo.weekOfYear + ' ' + documentName;
    }

    final String title =
        calcUniqueTitle(blueprintContext, adjustedDocumentName);
    blueprintContext.setTitle(title);
    final String sortKey = weekInfo.year + " #"
        + (weekInfo.weekOfYear < 10 ? "0" : "") + weekInfo.weekOfYear
        + (StringUtils.isNotBlank(documentName) ? ' ' + documentName
            : StringUtils.EMPTY);
    blueprintContext.put(BlueprintContextNames.SORT_KEY, sortKey);
    return adjustedDocumentName;
  }

  private String calcUniqueTitle(final BlueprintContext blueprintContext,
      final String original) {
    int counter = 1;
    String current = original;
    while (pageManager.getPage(blueprintContext.getSpaceKey(),
        current) != null) {
      current = original + " #" + counter;
      counter++;
    }
    return current;
  }

  // --- object basics --------------------------------------------------------

}
