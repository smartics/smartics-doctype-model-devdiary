/*
 * Copyright 2014-2018 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
require(['ajs', 'confluence/meta', 'tinymce', 'confluence/root'], function (AJS, Meta, tinymce, Confluence) {
  "use strict";

  const macroName = 'projectdoc-devdiary-rating';
  const updateMacro = function (macroNode, rating) {
    AJS.Rte.BookmarkManager.storeBookmark();
    const macroRenderRequest = {
      contentId: Meta.get('content-id') || 0,
      macro: {
        name: macroName,
        defaultParameterValue: "",
        params: {"value": rating},
        body: ""
      }
    };
    tinymce.confluence.MacroUtils.insertMacro(macroRenderRequest);
  };

  // AJS.bind("add-handler.property-panel", function (event, panel) {

    const eventSource = /* panel.registerButtonHandler ? panel : */ Confluence.PropertyPanel.Macro;

    if (eventSource.registerButtonHandler) {
      eventSource.registerButtonHandler("projectdoc-10-perfect", function (e, macroNode) {
        updateMacro(macroNode, "10-perfect");
      });
      eventSource.registerButtonHandler("projectdoc-9-good-up", function (e, macroNode) {
        updateMacro(macroNode, "9-good-up");
      });
      eventSource.registerButtonHandler("projectdoc-8-good", function (e, macroNode) {
        updateMacro(macroNode, "8-good");
      });

      eventSource.registerButtonHandler("projectdoc-7-good-down", function (e, macroNode) {
        updateMacro(macroNode, "7-good-down");
      });
      eventSource.registerButtonHandler("projectdoc-6-ok-up", function (e, macroNode) {
        updateMacro(macroNode, "6-ok-up");
      });
      eventSource.registerButtonHandler("projectdoc-five-ok", function (e, macroNode) {
        updateMacro(macroNode, "five-ok");
      });
      eventSource.registerButtonHandler("projectdoc-4-ok-down", function (e, macroNode) {
        updateMacro(macroNode, "4-ok-down");
      });
      eventSource.registerButtonHandler("projectdoc-3-imperfect-up", function (e, macroNode) {
        updateMacro(macroNode, "3-imperfect-up");
      });
      eventSource.registerButtonHandler("projectdoc-2-imperfect", function (e, macroNode) {
        updateMacro(macroNode, "2-imperfect");
      });
      eventSource.registerButtonHandler("projectdoc-1-imperfect-down", function (e, macroNode) {
        updateMacro(macroNode, "1-imperfect-down");
      });
      eventSource.registerButtonHandler("projectdoc-zero-unrated", function (e, macroNode) {
        updateMacro(macroNode, "zero-unrated");
      });
    }
  // });
});

require(['ajs', 'confluence/meta', 'tinymce', 'confluence/root'], function (AJS, Meta, tinymce, Confluence) {
  "use strict";

  const macroName = 'projectdoc-devdiary-shortrating';
  const updateShortMacro = function (macroNode, rating) {
    AJS.Rte.BookmarkManager.storeBookmark();
    const macroRenderRequest = {
      contentId: Meta.get('content-id') || 0,
      macro: {
        name: macroName,
        defaultParameterValue: "",
        params: {"value": rating},
        body: ""
      }
    };
    tinymce.confluence.MacroUtils.insertMacro(macroRenderRequest);
  };

  Confluence.PropertyPanel.Macro.registerButtonHandler("projectdoc-4-perfect", function (e, macroNode) {
    updateShortMacro(macroNode, "4-perfect");
  });
  Confluence.PropertyPanel.Macro.registerButtonHandler("projectdoc-3-good", function (e, macroNode) {
    updateShortMacro(macroNode, "3-good");
  });
  Confluence.PropertyPanel.Macro.registerButtonHandler("projectdoc-2-ok", function (e, macroNode) {
    updateShortMacro(macroNode, "2-ok");
  });
  Confluence.PropertyPanel.Macro.registerButtonHandler("projectdoc-1-imperfect", function (e, macroNode) {
    updateShortMacro(macroNode, "1-imperfect");
  });
  Confluence.PropertyPanel.Macro.registerButtonHandler("projectdoc-0-unrated", function (e, macroNode) {
    updateShortMacro(macroNode, "0-unrated");
  });
});
