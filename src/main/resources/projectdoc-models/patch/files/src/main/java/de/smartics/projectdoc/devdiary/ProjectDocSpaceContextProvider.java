/*
 * Copyright 2014-2018 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.devdiary;

import com.atlassian.confluence.plugins.createcontent.api.contextproviders.AbstractBlueprintContextProvider;
import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;
import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContextKeys;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Provides information for the space.
 */
public class ProjectDocSpaceContextProvider
    extends AbstractBlueprintContextProvider {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  protected BlueprintContext updateBlueprintContext(
      final BlueprintContext blueprintContext) {
    final String name =
        ObjectUtils.toString(blueprintContext.get("name"), (String) null);
    if (StringUtils.isNotBlank(name)) {
      blueprintContext.setTitle(name);
    }

    updateContextFinally(blueprintContext);

    return blueprintContext;
  }

  protected void updateContextFinally(final BlueprintContext blueprintContext) {
    // FIX override; otherwise we would need to override it in the JavaScript,
    // but then Confluence will take the value from the JavaScript and not the
    // one calculated via the blueprint context provider.
    blueprintContext.getMap().put("title",
        blueprintContext.get(BlueprintContextKeys.CONTENT_PAGE_TITLE.key()));
  }

  // --- object basics --------------------------------------------------------

}
