/*
 * Copyright 2014-2018 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.devdiary;

import de.smartics.projectdoc.atlassian.confluence.admin.space.SpaceAdminConsole;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.BlueprintContextNames;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderHelper;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderSupportService;
import de.smartics.projectdoc.atlassian.confluence.util.PageTitleReplacements;

import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.core.DateFormatter;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.user.User;

import org.apache.commons.lang3.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Provides information for a month. Since the user does not provide the name
 * and short description required by all projectdoc documents, this information
 * is calculated.
 */
public class ProjectDocMonthContextProvider
    extends AbstractProjectDocContextProvider {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * The identifier to reference the current user's full name.
   */
  public static final String USER_FULL_NAME =
      "projectdoc_doctype_common_userFullName";

  // --- members --------------------------------------------------------------

  private final ContentPropertyManager contentPropertyManager;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public ProjectDocMonthContextProvider(
      final ContextProviderSupportService support,
      final ContentPropertyManager contentPropertyManager) {
    super(support);
    this.contentPropertyManager = contentPropertyManager;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  protected BlueprintContext updateBlueprintContext(
      final BlueprintContext blueprintContext) {
    final User user = AuthenticatedUserThreadLocal.get();
    if (user != null) {
      final ContextProviderHelper handler = createHandler(blueprintContext);
      final String team =
          handler.getRenderedNormalizedPropertyByKey(BlueprintContextNames.TEAM, null);
      final String userFullname =
          StringUtils.isNotBlank(team) ? team : user.getFullName();
      blueprintContext.put(USER_FULL_NAME, userFullname);

      final String userName =
          StringUtils.isNotBlank(team) ? team : user.getName();
      final DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
      final Date date = createDate(blueprintContext);
      final String titleBase =
          Objects.toString(blueprintContext.get(BlueprintContextNames.NAME));
      final String formattedDate = format.format(date);


      if (StringUtils.isBlank(Objects
          .toString(blueprintContext.get(BlueprintContextNames.SHORT_DESCRIPTION), null))) {
        final I18NBean i18n = this.user.createI18n();
        final String shortDescriptionLabel =
            i18n.getText("projectdoc.doctype.month.shortDescription.label",
                new Object[] {userFullname, formattedDate});
        blueprintContext.put(BlueprintContextNames.SHORT_DESCRIPTION, shortDescriptionLabel);
      }

      final MonthInfo monthInfo = addQueryParameterValues(blueprintContext,
          format, date, formattedDate, userName);
      final String name = calcDocumentNameAndSetTitleAndSortKey(
          blueprintContext, monthInfo, titleBase, date);
      blueprintContext.put(BlueprintContextNames.NAME, name);

      final DateFormatter formatter = this.user.getDateFormatter();
      final String dateString = formatter.format(date);
      blueprintContext.put(BlueprintContextNames.CREATION_DATE, dateString);
      final String timeString = formatter.formatTime(date);
      blueprintContext.put(BlueprintContextNames.CREATION_TIME, timeString);

      addSpaceKeyElementContext(blueprintContext);
    }

    updateContextFinally(blueprintContext);

    return blueprintContext;
  }

  private Date createDate(final BlueprintContext blueprintContext) {
    final Object dateObject = blueprintContext.get("day-date");

    final Date date;
    if (dateObject != null) {
      final DateFormat format = new SimpleDateFormat("yy-MM-dd");
      try {
        date = format.parse(String.valueOf(dateObject));
      } catch (final ParseException e) {
        return calcFirstDayOfMonth(new Date());
      }
    } else {
      date = new Date();
    }

    return calcFirstDayOfMonth(date);
  }

  private Date calcFirstDayOfMonth(final Date date) {
    final Calendar calendar = user.createCalendar();
    calendar.setTime(date);
    calendar.set(Calendar.DAY_OF_MONTH, 1);
    return calendar.getTime();
  }

  private MonthInfo addQueryParameterValues(
      final BlueprintContext blueprintContext, final DateFormat dashedFormat,
      final Date date, final String formattedDate, final String userName) {
    final Calendar calendar = user.createCalendar();
    calendar.setTime(date);
    final int monthOfYear = calendar.get(Calendar.MONTH) + 1;

    final String monthOfYearString = String.valueOf(monthOfYear);
    blueprintContext.put("projectdoc_doctype_month_number", monthOfYearString);

    final DateFormat format = new SimpleDateFormat("yyyyMMdd");
    calendar.set(Calendar.DAY_OF_MONTH, 1);
    final Date monthStart = calendar.getTime();
    final String monthStartString = format.format(monthStart);
    calendar.set(Calendar.DAY_OF_MONTH,
        calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
    final Date monthEnd = calendar.getTime();
    final String monthEndString = format.format(monthEnd);
    blueprintContext.put("projectdoc_month_firstDay", monthStartString);
    blueprintContext.put("projectdoc_month_lastDay", monthEndString);

    addJiraQueryParameterValues(blueprintContext, dashedFormat, monthStart,
        monthEnd, userName);

    final MonthInfo weekInfo =
        new MonthInfo(calendar, monthOfYear, monthStart, monthEnd);
    return weekInfo;
  }

  private void addJiraQueryParameterValues(
      final BlueprintContext blueprintContext, final DateFormat format,
      final Date weekStart, final Date weekEnd, final String userName) {
    final String spaceKey = blueprintContext.getSpaceKey();
    final SpaceAdminConsole console =
        spaceAdminCenter.createConsoleFor(spaceKey);

    final String jiraName = console.getPropertyAsString("jira.name");
    if (jiraName != null) {
      blueprintContext.put("projectdoc_jira_name", jiraName);

      blueprintContext.put("projectdoc_user_id", userName);

      final String weekStartString = format.format(weekStart);
      final String weekEndString = format.format(weekEnd);
      blueprintContext.put("projectdoc_jira_startDate", weekStartString);
      blueprintContext.put("projectdoc_jira_endDate", weekEndString);
    }
  }

  private static final class MonthInfo {
    private final Calendar calendar;

    private final int monthOfYear;

    private final Date monthStart;
    private final Date monthEnd;

    private final String year;

    private MonthInfo(final Calendar calendar, final int monthOfYear,
        final Date monthStart, final Date monthEnd) {
      this.calendar = calendar;
      this.monthOfYear = monthOfYear;
      this.monthStart = monthStart;
      this.monthEnd = monthEnd;
      this.year = calcYear();
    }

    private String calcYear() {
      calendar.setTime(monthStart);
      int year = calendar.get(Calendar.YEAR);
      return String.valueOf(year);
    }
  }

  private String calcDocumentNameAndSetTitleAndSortKey(
      final BlueprintContext blueprintContext, final MonthInfo monthInfo,
      final String documentName, final Date date) {
    final String adjustedDocumentName;
    if (documentName.indexOf('@') != -1) {
      final Page stub = new Page();
      final Map<String, Object> context = new HashMap<>();

      final Long parentPageId = getAsLong(blueprintContext, "parentPageId");
      if (parentPageId != null) {
        final Page parentPage = pageManager.getPage(parentPageId);
        if (parentPage != null) {
          stub.setParentPage(parentPage);
          supplyTeamToContext(context, parentPage);
        }
      }
      final Space space = spaceManager.getSpace(blueprintContext.getSpaceKey());
      stub.setSpace(space);

      stub.setTitle(documentName);
      final PageTitleReplacements replacer =
          new PageTitleReplacements(spaceManager, contentPropertyManager);
      adjustedDocumentName = replacer.replaceAll(stub, context, date, user);
    } else {
      adjustedDocumentName =
          monthInfo.year + " #" + monthInfo.monthOfYear + ' ' + documentName;
    }

    final String title =
        calcUniqueTitle(blueprintContext, adjustedDocumentName);
    blueprintContext.setTitle(title);
    final String sortKey = monthInfo.year + " #"
        + (monthInfo.monthOfYear < 10 ? "0" : "") + monthInfo.monthOfYear;
    blueprintContext.put(BlueprintContextNames.SORT_KEY, sortKey);
    return adjustedDocumentName;
  }

  private String calcUniqueTitle(final BlueprintContext blueprintContext,
      final String original) {
    int counter = 1;
    String current = original;
    while (pageManager.getPage(blueprintContext.getSpaceKey(),
        current) != null) {
      current = original + " #" + counter;
      counter++;
    }
    return current;
  }

  // --- object basics --------------------------------------------------------

}
