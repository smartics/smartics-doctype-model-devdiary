/*
 * Copyright 2014-2018 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.devdiary.macros;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.sal.api.message.I18nResolver;

import de.smartics.projectdoc.atlassian.confluence.util.EncodingUtils;
import de.smartics.projectdoc.atlassian.confluence.util.UnifiedApiMacro;

/**
 * A macro to help editing values of enumerations.
 */
public abstract class SelectionMacro extends UnifiedApiMacro {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  /**
   * The name to the parameter to specify the selected enumeration value.
   * <p>
   * The value of this constant is {@value}.
   * </p>
   */
  private static final String PARAM_NAME_ENUM_VALUE = "value";

  // --- members --------------------------------------------------------------

  /**
   * Provides access to localized information.
   */
  private final I18nResolver i18n;

  private final SettingsManager settingsManager;

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  protected SelectionMacro(final I18nResolver i18n,
      final SettingsManager settingsManager) {
    this.i18n = i18n;
    this.settingsManager = settingsManager;
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  @Override
  public RenderMode getBodyRenderMode() {
    return RenderMode.NO_RENDER;
  }

  @Override
  public boolean hasBody() {
    return false;
  }

  @Override
  public BodyType getBodyType() {
    return BodyType.NONE;
  }

  @Override
  public OutputType getOutputType() {
    return OutputType.INLINE;
  }

  @Override
  @SuppressWarnings({"rawtypes"})
  public TokenType getTokenType(final Map parameters, final String body,
      final RenderContext context) {
    return TokenType.INLINE;
  }

  @Override
  public boolean isInline() {
    return true;
  }

  // --- business -------------------------------------------------------------

  @Override
  public String execute(final Map<String, String> parameters, final String body,
      final ConversionContext conversionContext)
      throws MacroExecutionException {
    final String value =
        getParameter(parameters, PARAM_NAME_ENUM_VALUE, getDefaultValue());

    if (StringUtils.isEmpty(value)) {
      return value;
    }

    final String type = getName();
    final String key =
        "de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-devdiary."
            + type + ".param.value.value." + value + ".label";
    final String label = i18n != null ? i18n.getText(key) : value;

    final String content = label != null ? label : value;

    if (StringUtils.isEmpty(content)) {
      return content;
    } else {
      final String location = getImageLocation(parameters, conversionContext);
      if (location != null) {
        final StringBuilder buffer = new StringBuilder(128);
        buffer.append("<img src=\"")
            .append(settingsManager.getGlobalSettings().getBaseUrl())
            .append('/')
            .append(location)
            .append('"');

        final String strippedContentForAttributeValue =
            EncodingUtils.encodeHtmlAttributeValue(content);
        buffer.append(" class=\"")
            .append(type)
            .append(' ')
            .append(strippedContentForAttributeValue)
            .append('"');
        buffer.append(" alt=\"")
            .append(strippedContentForAttributeValue)
            .append('"');
        if (i18n != null) {
          final String keyDesc =
              "de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-devdiary."
                  + type + ".param.value.value." + value + ".desc";
          final String title = i18n.getText(keyDesc);
          buffer.append(" title=\"").append(title).append('"');
        }

        buffer.append("></img>");
        return buffer.toString();
      } else {
        return StringUtils.EMPTY;
      }
    }
  }

  @SuppressWarnings({"rawtypes"})
  protected abstract String getImageLocation(Map params, ConversionContext ctx);

  protected String getDefaultValue() {
    return "";
  }

  protected String getName() {
    return "projectdoc-devdiary-selection";
  }

  public static String getParameter(final Map<String, String> parameters,
      final String name, final String defaultValue) {
    final String value = parameters.get(name);
    if (value == null) {
      return defaultValue;
    } else {
      return value;
    }
  }

  // --- object basics --------------------------------------------------------

}
