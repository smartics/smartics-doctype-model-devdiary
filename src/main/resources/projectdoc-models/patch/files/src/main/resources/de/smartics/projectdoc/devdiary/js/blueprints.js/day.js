require(['ajs', 'confluence/root', 'de/smartics/projectdoc/modules/core'], function (AJS, Confluence, PROJECTDOC) {
  "use strict";

  AJS.bind("blueprint.wizard-register.ready", function () {
    Confluence.Blueprint.setWizard(
      'de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-devdiary:create-doctype-template-day', function (wizard) {
        wizard.on('pre-render.page1Id', function (e, state) {
          state.wizardData.selectedWebItemData = PROJECTDOC.prerenderWizard(e, state);
        });

        wizard.on("post-render.page1Id", function (e, state) {
          AJS.$('#day-date').datePicker({
            overrideBrowserDefault: true,
            dateFormat: "yy-mm-dd"
          });

          AJS.$('#day-date').attr('placeholder', AJS.I18n.getText('projectdoc.doctype.day.wizard.placeholder'));

          PROJECTDOC.postrenderWizard(e, state);
        });

        wizard.on('submit.page1Id', function (e, state) {
          PROJECTDOC.adjustToLocation(state);
        });

        wizard.assembleSubmissionObject = PROJECTDOC.assemblePageCreationData;
      });
  });
});