/*
 * Copyright 2014-2018 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.devdiary;

import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.user.User;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import de.smartics.projectdoc.atlassian.confluence.admin.space.SpaceAdminConsole;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.BlueprintContextNames;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderHelper;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderSupportService;
import org.apache.commons.lang3.StringUtils;

import java.text.DateFormat;
// import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

/**
 * Provides information for a day. Since the user does not provide the name and
 * short description required by all projectdoc documents, this information is
 * calculated.
 */
public class ProjectDocDayContextProvider
    extends AbstractProjectDocContextProvider {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

//  private static final Logger LOG =
//      LoggerFactory.getLogger(ProjectDocDayContextProvider.class);

  /**
   * The identifier to reference the current user's full name.
   */
  public static final String USER_FULL_NAME =
      "projectdoc_doctype_common_userFullName";

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public ProjectDocDayContextProvider(
      final ContextProviderSupportService support) {
    super(support);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  protected BlueprintContext updateBlueprintContext(
      final BlueprintContext blueprintContext) {
    final ContextProviderHelper handler = createHandler(blueprintContext);
    addPropertiesToContext(handler);

    final User user = AuthenticatedUserThreadLocal.get();
    if (user != null) {
      final String team =
          handler.getRenderedNormalizedPropertyByKey(BlueprintContextNames.TEAM,
              null);
      final String userFullname =
          StringUtils.isNotBlank(team) ? team : user.getFullName();
      blueprintContext.put(USER_FULL_NAME, userFullname);

      final String userName =
          StringUtils.isNotBlank(team) ? team : user.getName();
      final DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
      final String titleBase = userName;
      final Date today = (Date) blueprintContext
          .get(ContextProviderHelper.CREATION_DATE_INSTANCE);
      final String formattedDate = format.format(today);
      adjustTitle(blueprintContext, titleBase, format, formattedDate);

      final I18NBean i18n = this.user.createI18n();
      final String nameLabel = i18n.getText("projectdoc.doctype.day.name.label",
          new Object[] {userFullname, formattedDate});
      blueprintContext.put(BlueprintContextNames.NAME, nameLabel);
      if (StringUtils.isBlank(Objects.toString(
          blueprintContext.get(BlueprintContextNames.SHORT_DESCRIPTION),
          null))) {
        final String shortDescriptionLabel =
            i18n.getText("projectdoc.doctype.day.name.label",
                new Object[] {userFullname, formattedDate});
        blueprintContext.put(BlueprintContextNames.SHORT_DESCRIPTION,
            shortDescriptionLabel);
      }

      addJiraQueryParameterValues(blueprintContext, format, formattedDate,
          userName);
    }

    updateContextFinally(blueprintContext);

    return blueprintContext;
  }

/*
  private String toDateString(final DateFormat format,
      final Object dateObject) {
    if (dateObject instanceof String) {
      return (String) dateObject;
    } else {
      final Date date;
      if (dateObject instanceof Date) {
        date = (Date) dateObject;
      } else {
        date = new Date();
      }
      return format.format(date);
    }
  }

  private Date toDate(final DateFormat format, final Object dateObject) {
    if (dateObject instanceof String) {
      try {
        return format.parse((String) dateObject);
      } catch (final ParseException e) {
        LOG.error("Cannot parse date string '{}' -- {}", dateObject,
            e.getMessage());
        return new Date();
      }
    } else if (dateObject instanceof Date) {
      return (Date) dateObject;
    } else {
      return new Date();
    }
  }
*/

  private void addJiraQueryParameterValues(
      final BlueprintContext blueprintContext, final DateFormat format,
      final String formattedToday, final String userName) {
    final String spaceKey = blueprintContext.getSpaceKey();
    final SpaceAdminConsole console =
        spaceAdminCenter.createConsoleFor(spaceKey);

    final String jiraName = console.getPropertyAsString("jira.name");
    if (jiraName != null) {
      blueprintContext.put("projectdoc_jira_name", jiraName);

      blueprintContext.put("projectdoc_user_id", userName);

      final Date today = (Date) blueprintContext
          .get(ContextProviderHelper.CREATION_DATE_INSTANCE);
      final Calendar calendar = Calendar.getInstance();
      calendar.setTime(today);
      calendar.add(Calendar.DATE, 1);
      final Date tomorrow = calendar.getTime();
      final String formattedTomorrow = format.format(tomorrow);
      blueprintContext.put("projectdoc_jira_today", formattedToday);
      blueprintContext.put("projectdoc_jira_tomorrow", formattedTomorrow);
    }
  }

  private void adjustTitle(final BlueprintContext blueprintContext,
      final String name, final DateFormat format, final String dateString) {
    final String documentName = dateString + ' ' + name;
    final String title = calcUniqueTitle(blueprintContext, documentName);
    blueprintContext.setTitle(title);
    blueprintContext.put(BlueprintContextNames.SORT_KEY, title);
  }

  private String calcUniqueTitle(final BlueprintContext blueprintContext,
      final String original) {
    int counter = 1;
    String current = original;
    while (pageManager.getPage(blueprintContext.getSpaceKey(), current) !=
           null) {
      current = original + " #" + counter;
      counter++;
    }
    return current;
  }

  // --- object basics --------------------------------------------------------

}
