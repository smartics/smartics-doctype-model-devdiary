/*
 * Copyright 2014-2018 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.devdiary.macros;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.atlassian.confluence.content.render.image.ImageDimensions;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.DefaultImagePlaceholder;
import com.atlassian.confluence.macro.EditorImagePlaceholder;
import com.atlassian.confluence.macro.ImagePlaceholder;
import com.atlassian.renderer.v2.macro.ResourceAware;

import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.sal.api.message.I18nResolver;

/**
 * Allows to select an rating for a resource (or maybe other items that require
 * to be rated).
 */
public class SelectionMacroRating extends SelectionMacro implements
    EditorImagePlaceholder, ResourceAware
{
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  private static final String IMAGE_PATH =
      "/download/resources/de.smartics.atlassian.confluence.smartics-projectdoc-confluence-space-devdiary/macro-icons/";

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public SelectionMacroRating(final I18nResolver i18n,
      final SettingsManager settingsManager) {
    super(i18n, settingsManager);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  @Override
  protected String getName()
  {
    return "projectdoc-devdiary-rating";
  }

  // --- business -------------------------------------------------------------

  @Override
  public ImagePlaceholder getImagePlaceholder(final Map<String, String> params,
      final ConversionContext ctx)
  {
    final String value = params.get("value");
    if (StringUtils.isBlank(value))
    {
      return null;
    }
    final DefaultImagePlaceholder placeholder =
        new DefaultImagePlaceholder(createImagePath(value), true, new ImageDimensions(40,
            40));
    return placeholder;
  }

  private static String createImagePath(final String value)
  {
    return IMAGE_PATH + "rating-" + value + ".png";
  }

  @Override
  @SuppressWarnings({ "rawtypes" })
  public String getImageLocation(final Map params, final ConversionContext ctx)
  {
    final String value = (String) params.get("value");
    if (StringUtils.isBlank(value))
    {
      return null;
    }
    return createImagePath(value);
  }

  @Override
  public boolean suppressMacroRenderingDuringWysiwyg()
  {
    return true;
  }

  @Override
  public boolean suppressSurroundingTagDuringWysiwygRendering()
  {
    return false;
  }

  @Override
  public String getResourcePath()
  {
    return "";
  }

  @Override
  public void setResourcePath(final String resourcePath)
  {
  }

  // --- object basics --------------------------------------------------------

}
