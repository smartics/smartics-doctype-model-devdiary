/*
 * Copyright 2013-2025 Kronseder & Reiner GmbH, smartics
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.devdiary.app.config;

import de.smartics.projectdoc.devdiary.ProjectDocContextProvider;
import de.smartics.projectdoc.devdiary.ProjectDocDayContextProvider;
import de.smartics.projectdoc.devdiary.ProjectDocWeekContextProvider;
import de.smartics.projectdoc.devdiary.ProjectDocMonthContextProvider;
import de.smartics.projectdoc.devdiary.ProjectDocSpaceContextProvider;
import de.smartics.projectdoc.devdiary.ProjectDocSpacePageContextProvider;
import de.smartics.projectdoc.devdiary.ProjectDocContextProvider;
import de.smartics.projectdoc.devdiary.subspace.PartitionContextProvider;
import de.smartics.projectdoc.devdiary.subspace.SubspaceContextProvider;
import com.atlassian.confluence.core.ContentPropertyManager;
import de.smartics.projectdoc.confluence.tool.blueprint.partition.PartitionContextProviderSupportService;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderSupportService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({ConfluenceComponentImports.class, ProjectdocComponentImports.class,
    AppConfluenceComponentImports.class})
public class ServicesConfig {
  @Bean
  public ProjectDocContextProvider projectDocContextProvider(
      final ContextProviderSupportService support) {
    return new ProjectDocContextProvider(support);
  }

  @Bean
  public ProjectDocDayContextProvider projectDocdayContextProvider(
      final ContextProviderSupportService support) {
    return new ProjectDocDayContextProvider(support);
  }

  @Bean
  public ProjectDocWeekContextProvider projectDocWeekContextProvider(
      final ContextProviderSupportService support,
      final ContentPropertyManager contentPropertyManager) {
    return new ProjectDocWeekContextProvider(support, contentPropertyManager);
  }

  @Bean
  public ProjectDocMonthContextProvider projectDocMonthContextProvider(
      final ContextProviderSupportService support,
      final ContentPropertyManager contentPropertyManager) {
    return new ProjectDocMonthContextProvider(support, contentPropertyManager);
  }

  @Bean
  public ProjectDocSpaceContextProvider projectDocSpaceContextProvider() {
    return new ProjectDocSpaceContextProvider();
  }

  @Bean
  public ProjectDocSpacePageContextProvider projectDocSpacePageContextProvider() {
    return new ProjectDocSpacePageContextProvider();
  }

  @Bean
  public SubspaceContextProvider subspaceContextProvider(
      final PartitionContextProviderSupportService support) {
    return new SubspaceContextProvider(support);
  }
}
